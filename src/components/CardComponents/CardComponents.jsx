import React, { Component } from 'react';
import {Box, Card, CardHeader, CardBody,CardFooter,} from "grommet";
import {Achievement, Organization, Services} from "grommet-icons";
import {BrowserRouter as Router, Switch, Route, Link, NavLink} from "react-router-dom";
import img from '../../img/AboutImage.jpg'




class CardComponents extends Component {

    render() {
        return(
            <Router>
                <Box direction={"row"} justify={"center"} className={'BoxCard'} background={"url(" + img +")"}>
                    <Link to={"/Formation"}>
                        <Card  margin={'large'} height="small" className={'LinGrad3'}>
                            <CardHeader pad="medium"><Achievement className={'Icones'}/></CardHeader>
                            <CardBody pad="medium">Mes formations</CardBody>
                        </Card>
                    </Link>
                    <Link to={"/Experiences-Pro"}>
                        <Card  margin={"large"} height="small"  className={'LinGrad3'}>
                            <CardHeader pad="medium"><Organization className={'Icones'}/></CardHeader>
                            <CardBody pad="medium">Mes expériences professionnelles</CardBody>
                        </Card>
                    </Link>

                </Box>
            </Router>
        )
    }
}

export default CardComponents;

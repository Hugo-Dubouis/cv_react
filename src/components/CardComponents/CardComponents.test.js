import React from 'react';
import { shallow } from 'enzyme';
import CardComponents from './CardComponents';

describe('CardComponents', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<CardComponents />);
    expect(wrapper).toMatchSnapshot();
  });
});

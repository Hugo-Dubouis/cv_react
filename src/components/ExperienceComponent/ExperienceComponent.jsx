import React, { Component } from 'react';
import {Box, Heading} from "grommet";
import {Chrono} from "react-chrono"
import HeaderComponent from "../HeaderComponent";
import FooterComponent from "../FooterComponent";

const data = [
  {
    title: "avril 2017 - juin 2017",
    contentTitle: "Itelcom ",
    contentText: "Stage de fin de DUT",
    contentDetailedText: `J'ai effectué un stage dans l'entreprise Itelcom, situé à Raon-l'Etape dans les vosges, afin 
    de validé mon diplome pour le DUT. L'entreprise est spécialisée dans la permanence téléphonique. J
    'ai été amené à développer et mettre en place en système de communication interne.
     Comme il s'agissait pour moi des mes premiers pas en entreprise ce stage m'a permis de découvrir le monde de l'entreprise.`
  },
  {
    title: "Octobre 2018 - Aout 2019",
    contentTitle: "Evok design ",
    contentText:
        "Alternance licence pro",
    contentDetailedText: `J'ai eu la chance de découvrir l'alternance pour la suite de mes études. C'est un type de
     formation très intéressante où l'on peut se perfectionner dans différents domaines. J'ai réalisé ma première alternance dans l'agence 
     d'Evok design basé à Ludres près de Nancy. J'ai durant cette année d'alternance réalisé des sites web et élaborer un CMS maison. 
     Cette année à été très enrichissante pour moi et m'a beaucoup apris.`
  },
  {
    title: "Septembre 2019 - Décembre 2019",
    contentTitle: "Tel-on",
    contentText:
        "Alternance Master I",
    contentDetailedText: `J'ai ensuite continué mon parcours professionnel au sein 
    de l'entreprise Tel-on qui est spécialisé dans le monde de la téléphonie. J'ai réalisé différents site pour l'entreprise.
    Je n'y suis pas résté longtemps car j'ai eu une propsition intéressante pour la suite de mon parcours mais cela reste une bonne éxperience à mes yeux.`
  },
  {
    title: "Janvier 2020 - à ce jour ",
    contentTitle: "Sogeti",
    contentText:
        "Alternance Master",
    contentDetailedText: `Pour finir mon alternance pour mon master j'ai eu la chance de rejoindre Sogeti qui appartient au groupe Capgamini, leader du développement informatique. Cette alternance est très enrichissante
    . J'ai la chance de travailler sur des sujets très variés ( Web, devOps, méthodes agiles ...). cette alternance est une très bonne opportunité pour moi et pour la suite de mon parcours professionel.`
  },
];

class ExperienceComponent extends Component {

  render() {

    let ExpPro

    ExpPro=
        <Box>
          <Box style={{ width: "100%", height: "1000px" }}>
            <Chrono items={data} mode="TREE" theme={{primary: "#1d279a", secondary: "white" }} />
          </Box>
        </Box>

    return (
        <Box>
          <HeaderComponent></HeaderComponent>
          <Box>
            <Heading level={3} margin={'large'}>Mon Parcours professionnel</Heading>
            {ExpPro}
          </Box>
          <FooterComponent></FooterComponent>
        </Box>

    )
  }
}

export default ExperienceComponent;

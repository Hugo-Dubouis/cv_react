import React from 'react';
import { shallow } from 'enzyme';
import ExperienceComponent from './ExperienceComponent';

describe('ExperienceComponent', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<ExperienceComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});

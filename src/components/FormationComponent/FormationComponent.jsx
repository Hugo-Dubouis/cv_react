import React, {Component} from 'react';
import HeaderComponent from "../HeaderComponent";
import {Box, Heading} from "grommet";
import FooterComponent from "../FooterComponent";
import {Chrono} from "react-chrono"

const data = [
    {
        title: "2010 - 2013",
        contentTitle: "Lycée ",
        contentText:
            "Beau jardin - Saint-Dié-des-Vosges - 88100",
        contentDetailedText: `J'ai effectué un baccalauréat série S option SVT dans les Vosges au Lycée privé Beau-Jardin.
         J'ai obtenu mon bac en 2013.`
    },
    {
        title: "2013 - 2014",
        contentTitle: "Première année de médeçine ",
        contentText:
            "Faculté de médeçine de Brabois - Nancy - 54000",
        contentDetailedText: `Après mon bac j'ai décidé de suivre des études de médeçine, car je ne savais pas trop dans
         quel domaine m'orienter et c'était un peu le choix par défaut. Mais par manque de motivation ce ne fut pas une année de réussite et j'ai décidé de me réorienter`
    },
    {
        title: "2015 - 2017",
        contentTitle: "DUT informatique",
        contentText:
            "IUT Saint-Dié-Des-Vosges - 88100",
        contentDetailedText: `J'ai décidé de poursuivre mes études dans un domaine qui m'intérressais depuis que je suis jeune,
         l'informatique. C'est pourquoi je me suis tourné vers un DUT, une formation très pratique où en 2 ans on apprend 
         bien les bases de l'informatique dans tous les domaines. J'ai validé ce diplôme en 2017.`
    },
    {
        title: "2018 - 2019",
        contentTitle: "Licence proffesionnelle - CIASIE",
        contentText:
            "IUT Charlemagne - Nancy - 54000",
        contentDetailedText: `J'ai ensuite décidé de poursuivre ma formation sur une licence professionnelle CIASIE 
        ( Conception Intégration D'Application au Service de l'Entreprise ), que j'ai effectué à l'IUT Charlemagne à Nancy.
         J'ai pu grâce à cette licence découvrir le monde professionnel car je l'ai réalisé en alternance, ce qui m'a permis
          de monter en compétences sur de nombreux sujets différents, aussi bien techniques que humains. `
    },
    {
        title: "2019 - 2021",
        contentTitle: "Master expert informatique et système d’information",
        contentText:
            "Ynov - Toulouse - 31000",
        contentDetailedText: `Pour terminer mes études j'ai décidé de rejoindre l'école Ynov à Toulouse et donc de quitter
         ma région du Nord Est. J'ai choisi cette école car elle permettait également de réaliser le cursus en alternance
          et offre un programme pédagogique très riche et très professionnalisant. Le cursus nous permet de choisir une spécialisation,
           et c'est pour cela que j'ai choisi la spécialisation en développement web, car il s'agit du domaine qui m'a toujours le plus intéressé
            en informatique et dans lequel je souhaite commencer ma carrière professionnelle.`
    },
];

class FormationComponent extends Component {

    render() {

        let formation

        formation=
            <Box>
                <Box style={{ width: "100%", height: "1000px" }}>
                    <Chrono items={data} mode="TREE" theme={{primary: "brown", secondary: "white" }} />
                </Box>
            </Box>

        return (
            <Box>
                <HeaderComponent></HeaderComponent>
                <Box>
                    <Heading level={3} margin={'large'}>Mes formations</Heading>
                    {formation}
                </Box>
                <FooterComponent></FooterComponent>
            </Box>

        )
    }
}

export default FormationComponent;

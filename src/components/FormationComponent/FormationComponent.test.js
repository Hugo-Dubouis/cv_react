import React from 'react';
import { shallow } from 'enzyme';
import FormationComponent from './FormationComponent';

describe('FormationComponent', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<FormationComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});

import React, { Component } from 'react';
import {Box, Button, Text, Meter, Card, CardBody,CardFooter,Heading} from "grommet";
import {Close} from "grommet-icons";

class CompetenceComponent extends Component {

    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
        this.state = {rendered : ''}
    }
    handleClick(e){

        if(e.currentTarget.id === 'soft'){
            this.state.rendered = 'soft'
        }
        else if (e.currentTarget.id === 'hard'){
            this.state.rendered = 'hard'
        }
        else if (e.currentTarget.id === 'close'){
            this.state.rendered = ''
        }
        this.forceUpdate();
    }
    render() {

        let competences;

        if (this.state.rendered === 'soft'){
            competences =
                <Box>
                    <Button id={'close'} className={'BtnScdr'} icon={<Close/>} plain={false} onClick={this.handleClick} margin={{left:"small"}}></Button>
                    <Box
                    direction={'row'}
                    margin={"small"}>
                    <Card  height="small" width="small" background="light-1" margin={{right:"10px"}}>
                        <CardBody pad="medium">
                            <Meter
                                type={'circle'}
                                values={[{
                                    color: '#493F3D',
                                    value: 70,
                                    label: 'sixty',
                                    onClick: () => {}
                                }]}
                                aria-label="meter"
                            />
                        </CardBody>
                        <CardFooter pad={{horizontal: "small"}} background="#493F3D">
                            <Text>Optimisme</Text>
                        </CardFooter>
                    </Card>

                    <Card  height="small" width="small" background="light-1" margin={{right:"10px"}}>
                        <CardBody pad="medium">
                            <Meter
                                type={'circle'}
                                values={[{
                                    color: '#493F3D',
                                    value: 80,
                                    label: 'sixty',
                                    onClick: () => {}
                                }]}
                                aria-label="meter"
                            />
                        </CardBody>
                        <CardFooter pad={{horizontal: "small"}} background="#493F3D">
                            <Text>Adaptabilité</Text>
                        </CardFooter>
                    </Card>

                    <Card  height="small" width="small" background="light-1" margin={{right:"10px"}}>
                        <CardBody pad="medium">
                            <Meter
                                type={'circle'}
                                values={[{
                                    color: '#493F3D',
                                    value: 75,
                                    label: 'sixty',
                                    onClick: () => {}
                                }]}
                                aria-label="meter"
                            />
                        </CardBody>
                        <CardFooter pad={{horizontal: "small"}} background="#493F3D">
                            <Text>Rigueur</Text>
                        </CardFooter>
                    </Card>

                    <Card  height="small" width="small" background="light-1" margin={{right:"10px"}}>
                        <CardBody pad="medium">
                            <Meter
                                type={'circle'}
                                values={[{
                                    color: '#493F3D',
                                    value: 60,
                                    label: 'sixty',
                                    onClick: () => {}
                                }]}
                                aria-label="meter"
                            />
                        </CardBody>
                        <CardFooter pad={{horizontal: "small"}} background="#493F3D">
                            <Text>Esprit d'initiative</Text>
                        </CardFooter>
                    </Card>
                </Box>
                </Box>

        }
        if (this.state.rendered === 'hard'){
            competences =
                <Box>
                    <Button id={'close'} className={'BtnScdr'} icon={<Close/>} plain={false} onClick={this.handleClick} margin={{left:"small"}}></Button>
                    <Box
                    direction={'row'}
                    margin={"small"}>
                    <Card  height="small" width="small" background="light-1" margin={{right:"10px"}}>
                        <CardBody pad="medium">
                            <Meter
                                type={'circle'}
                                values={[{
                                    color: '#493F3D',
                                    value: 90,
                                }]}
                                aria-label="meter"
                            />
                        </CardBody>
                        <CardFooter pad={{horizontal: "small"}} background="#493F3D">
                            <Text>VueJs</Text>
                        </CardFooter>
                    </Card>

                    <Card  height="small" width="small" background="light-1" margin={{right:"10px"}}>
                        <CardBody pad="medium">
                            <Meter
                                type={'circle'}
                                values={[{
                                    color: '#493F3D',
                                    value: 83,
                                    label: 'sixty',
                                }]}
                                aria-label="meter"
                            />
                        </CardBody>
                        <CardFooter pad={{horizontal: "small"}} background="#493F3D">
                            <Text>Angular</Text>
                        </CardFooter>
                    </Card>

                    <Card  height="small" width="small" background="light-1" margin={{right:"10px"}}>
                        <CardBody pad="medium">
                            <Meter
                                type={'circle'}
                                values={[{
                                    color: '#493F3D',
                                    value: 70,
                                    label: 'sixty',
                                    onClick: () => {}
                                }]}
                                aria-label="meter"
                            />
                        </CardBody>
                        <CardFooter pad={{horizontal: "small"}} background="#493F3D">
                            <Text>Symfony 4</Text>
                        </CardFooter>
                    </Card>

                    <Card  height="small" width="small" background="light-1" margin={{right:"10px"}}>
                        <CardBody pad="medium">
                            <Meter
                                type={'circle'}
                                values={[{
                                    color: '#493F3D',
                                    value: 65,
                                    label: 'sixty',
                                    onClick: () => {}
                                }]}
                                aria-label="meter"
                            />
                        </CardBody>
                        <CardFooter pad={{horizontal: "small"}} background="#493F3D">
                            <Text>Devops</Text>
                        </CardFooter>
                    </Card>
                </Box>
                </Box>

            if(this.state.rendered === ''){
                competences = null
            }

        }
        return (
            <Box
                style={{paddingBottom: '30px'}}
                background={'#e8e4e2'}
                align={"center"}>
                <Heading level={3}>Mes compétences</Heading>
                <Box
                    direction={'row'}
                    margin={'small'}>
                    <Button id={'soft'} className={'Btn'} margin={{right: '10px'}} label={'Soft Skills'} onClick={this.handleClick} ></Button>
                    <Button id={'hard'} className={'Btn'} label={"Hard Skills"} onClick={this.handleClick}></Button>
                </Box>
                {competences}
            </Box>
        );
    }
}

export default CompetenceComponent;

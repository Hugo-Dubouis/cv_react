import React from 'react';
import { shallow } from 'enzyme';
import CompetenceComponent from './CompetenceComponent';

describe('CompetenceComponent', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<CompetenceComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});

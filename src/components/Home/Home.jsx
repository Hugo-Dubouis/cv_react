import React from 'react';
import {Box} from "grommet";
import HeaderComponent from "../HeaderComponent";
import FooterComponent from "../FooterComponent";
import AboutComponent from "../AboutComponent";
import CompetenceComponent from "../CompetenceComponent";
import CardComponents from "../CardComponents";

class Home extends React.Component{

    render(){
        return (
            <Box>
                <HeaderComponent></HeaderComponent>
                <AboutComponent></AboutComponent>
                <CompetenceComponent></CompetenceComponent>
                <CardComponents></CardComponents>
                <FooterComponent></FooterComponent>
            </Box>
        )
    }
}

export default Home;

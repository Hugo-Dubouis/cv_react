import React from 'react';
import { shallow } from 'enzyme';
import AboutComponent from './AboutComponent';

describe('AboutComponent', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<AboutComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});

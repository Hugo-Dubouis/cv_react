import React, { Component } from 'react';
import {Box, Button, Heading, Paragraph} from "grommet";
import {Download} from "grommet-icons";
import pdf from '../../img/CV_HugoDubouis.pdf'

class AboutComponent extends Component {

  render() {
    return (
        <Box
            className={'About'}
            margin={{top: "xxsmall"}}
            margin={{bottom:'large'}}
            align={"center"}
        >
          <Heading level={3} margin={{bottom: 'xsmall'}}>A propos de moi</Heading>
          <Paragraph className={'Aboutp'}>
            Je m'appelle Hugo Dubouis. J'ai 25 ans et je suis actuellement en Master II - développement
            web à l'école Ynov Toulouse. J'effectue ce cursus en alternance au sein du groupe Capgemini.
            J'ai toujours été passionné par l'informatique et plus particulièrement par le développement web ansi que par les solutions devOps.
          </Paragraph>
          <a  href={pdf} target={'blank'}><Button  className={'Btn'} margin={{top:'large'}} label={'Télécharger mon cv'} icon={<Download/>}></Button></a>
          <Paragraph margin={{top:'large'}} className={'Aboutp'}>
            Je suis également fan de sports et particulièrement de tennis et de golf que je pratique très régulièrement.
            J'aime aussi passer du temps à suivre le foot à la télévision et plus particulièrement le FC Barcelonne mon club de coeur depuis tout petit.
            Je passe également du temps à regarder des séries / films ou encore à jouer aux jeux-vidéos et principalement à League of Legends.
          </Paragraph>
        </Box>
    );
  }
}

export default AboutComponent;

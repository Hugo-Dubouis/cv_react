import React, { Component } from 'react';
import { Box, Avatar, Heading} from "grommet";
import imgHugo from '../../img/imgHugo.jpg'
import {Link} from "react-router-dom";


class HeaderComponent extends Component {
  render() {
    return (
        <Box
            style={{fontFamily : 'Sansita Swashed'}}
            direction={'column'}
            align={'center'}
            background={'#493F3D'}>
            <Heading level={1} alignSelf={"center"} margin={"small"}>Dubouis Hugo</Heading>
            <Link to={'/'}><Avatar className={'Avatar'} src={imgHugo} margin={{top : "small"}}/></Link>
            <Heading level={2}>Etudiant Ynov Toulouse - Master 2</Heading>
        </Box>
    );
  }
}

export default HeaderComponent;

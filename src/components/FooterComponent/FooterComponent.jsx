import React, { Component } from 'react';
import {Box, Button, Text} from "grommet";
import {FacebookOption, Twitter, Instagram, Linkedin} from "grommet-icons";

class FooterComponent extends Component {
  render() {
    return(
        <Box
            direction={'row'}
            justify={'center'}
            background={'#493F3D'}>
          <Button href={"https://www.facebook.com/hugo.dubouis/"} target={"blank"} margin={'small'}><FacebookOption/></Button>
          <Button href={"https://www.instagram.com/hugodubouis/"} target={"blank"} margin={'small'}><Instagram/></Button>
          <Button href={"https://twitter.com/Slywald"} target={"blank"} margin={'small'}><Twitter/></Button>
          <Button href={"https://www.linkedin.com/in/hugo-dubouis-43a5ba172/"} target={"blank"} margin={'small'}><Linkedin/></Button>
            <Text size={'small'} style={{fontStyle: 'italic'}} alignSelf={"center"}>Hugo Dubouis tous droits réservés ©</Text>
        </Box>
    )
  }
}

export default FooterComponent;

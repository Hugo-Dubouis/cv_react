import React from 'react';
import { shallow } from 'enzyme';
import FooterComponent from './FooterComponent';

describe('FooterComponent', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<FooterComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});

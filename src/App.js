import React from 'react';
import './App.css';
import Home from "./components/Home";
import FormationComponent from "./components/FormationComponent";
import {Box, Grommet} from "grommet";
import {BrowserRouter as Router, Switch, Route, Link, withRouter} from "react-router-dom";
import ExperienceComponent from "./components/ExperienceComponent";

const theme = {
  global: {
    font: {
      family: 'Source Sans Pro',
      size: '23px',
      height: '25px',
    },
  },
};


class App extends React.Component {

  render() {

    return (
        <Grommet theme={theme}>
            <Switch>
              <Route exact path={"/"}  component={withRouter(Home)}/>
              <Route path={"/Formation"}  component={withRouter(FormationComponent)}/>
              <Route path={"/Experiences-Pro"} component={withRouter(ExperienceComponent)}/>
            </Switch>
        </Grommet>
    );
  }
}

export default App;
